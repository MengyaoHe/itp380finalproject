// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "EscapePlanGameMode.generated.h"

UCLASS(minimalapi)
class AEscapePlanGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AEscapePlanGameMode();
};



