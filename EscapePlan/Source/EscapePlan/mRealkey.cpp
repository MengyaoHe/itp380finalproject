// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "mRealkey.h"


// Sets default values
AmRealkey::AmRealkey()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AmRealkey::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AmRealkey::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

