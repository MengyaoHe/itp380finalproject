// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "EnemyAIController.h"
#include "Enemy.h"
#include "Zombie.h"


void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();
	APawn* pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	MoveToActor(pawn);
	state = Start; 
	attackRange = 150.0f;
}

void AEnemyAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	switch (state)
	{
	case AEnemyAIController::Start:
		if (UGameplayStatics::GetPlayerPawn(this, 0))
			MoveToActor(UGameplayStatics::GetPlayerPawn(this, 0));
		state = Chase;
		break;
	case AEnemyAIController::Chase:
		if (UGameplayStatics::GetPlayerPawn(this, 0))
			MoveToActor(UGameplayStatics::GetPlayerPawn(this, 0));
		break;
	case AEnemyAIController::Attack:
		pawnPlayer = UGameplayStatics::GetPlayerPawn(this, 0);
		pawnEnemy = GetPawn();
		if (pawnEnemy != nullptr && pawnPlayer != nullptr) {
			dis = FVector::Dist(pawnPlayer->GetActorLocation(), pawnEnemy->GetActorLocation());
			if (dis > attackRange) {
				MoveToActor(pawnPlayer);
				state = Chase;
				
					//if (pawnEnemy->IsA<AZombie>)
					//{
						AZombie* zombie = Cast<AZombie>(pawnEnemy);
						zombie->StopAttack();
					//}
					//if (pawnEnemy->IsA<AEnemy>)
					//{
					//	AEnemy* dwarf = Cast<AEnemy>(pawnEnemy);
					//	dwarf->StartAttack();
					//}
			}
		}

		break;
	case AEnemyAIController::Dead:
		state = Dead;
		break;
	default:
		break;
	}
}

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	if (Result == EPathFollowingResult::Success) {
		state = Attack;
		//AEnemy* dwarf = Cast<AEnemy>(GetPawn());
		APawn* pawn = GetPawn();
		//if (pawn)
		{
			//if (pawn->IsA<AZombie>)
			//{
				AZombie* zombie = Cast<AZombie>(pawn);
				zombie -> StartAttack();
			//}
			//if (pawn->IsA<AEnemy>)
			//{
			//	AEnemy* dwarf = Cast<AEnemy>(pawn);
			//	dwarf->StartAttack();
			//}
		}
	}
}
