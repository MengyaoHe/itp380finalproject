// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "Zombie.h"
#include "EnemyAIController.h"


// Sets default values
AZombie::AZombie()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    ZombieMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ZombieMesh"));
    RootComponent = ZombieMesh;
    AIControllerClass = AEnemyAIController::StaticClass();
}

// Called when the game starts or when spawned
void AZombie::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AZombie::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AZombie::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}
void AZombie::StartAttack()
{
    float dur = PlayAnimMontage(AttackAnim);
    GetWorldTimerManager().SetTimer(AttackTimer, this, &AZombie::AttackHelper, dur - 0.25f, true);
}
void AZombie::AttackHelper()
{
    if (UGameplayStatics::GetPlayerPawn(this, 0)) {
        UGameplayStatics::GetPlayerPawn(this, 0)->TakeDamage(Damage, FDamageEvent(),
                                                             GetInstigatorController(), this);
    }
}

void AZombie::StopAttack()
{
    StopAnimMontage(AttackAnim);
    GetWorldTimerManager().ClearTimer(AttackTimer);
}

float AZombie::TakeDamage(float Damage, FDamageEvent const & DamageEvent,
                         AController * EventInstigator, AActor * DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
    
    if (ActualDamage > 0.0f) {
        Health -= ActualDamage;
        if (Health <= 0.0f) {
            // We're dead, don't allow further damage
            bCanBeDamaged = false;
            // TODO: Process death
            StopAttack();
            float duration = PlayAnimMontage(DeathAnim);
            GetWorldTimerManager().SetTimer(DwarfTimer, this, &AZombie::ToDie, duration - 0.25f);
            
            GetController()->UnPossess();
        }
    }
    
    return ActualDamage;
}

void AZombie::ToDie()
{
    Destroy();
}

