// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "Ammor.h"
#include "EscapePlanCharacter.h"


// Sets default values
AAmmor::AAmmor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    AmmoMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("AmmoMesh"));
    RootComponent = AmmoMesh;
}

// Called when the game starts or when spawned
void AAmmor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAmmor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    checkCollision();
}

void AAmmor::checkCollision(){
    FHitResult Hit(ForceInit);
    AEscapePlanCharacter* ac = Cast<AEscapePlanCharacter>(Hit.GetActor());
    if(ac){
        ac -> addAmmo();
        AmmoMesh -> Deactivate();
    }
}
