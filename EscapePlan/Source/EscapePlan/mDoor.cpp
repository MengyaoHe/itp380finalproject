// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "mDoor.h"


// Sets default values
AmDoor::AmDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    DoorMesh = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Door"));
    RootComponent = DoorMesh;
}

// Called when the game starts or when spawned
void AmDoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AmDoor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	TeleportPlayer();
}

void AmDoor::TeleportPlayer() {
	APawn* player = UGameplayStatics::GetPlayerPawn(this, 0);
	AActor* door = this;
	if (player && door)
	{
		float dis = FVector::Dist(player->GetActorLocation(), door->GetActorLocation());
		if (dis > -150.0 && dis < 150.0f)
		{
			FVector playLoc = player->GetActorLocation();
			if (playLoc.Y > door->GetActorLocation().Y)
			{
				playLoc.Y -= 300.0f;
			}
			else 
			{
				playLoc.Y += 300.0f;
			}
			player->SetActorLocation(playLoc, false);
		}
	}
}

