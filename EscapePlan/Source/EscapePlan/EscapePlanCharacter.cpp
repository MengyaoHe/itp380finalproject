// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "EscapePlan.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "EscapePlanCharacter.h"
#include "Weapon.h"

//////////////////////////////////////////////////////////////////////////
// AEscapePlanCharacter

AEscapePlanCharacter::AEscapePlanCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
	MyWeapon = nullptr;
    myHealth = maxHealth;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AEscapePlanCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AEscapePlanCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AEscapePlanCharacter::MoveRight);
    
    PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AEscapePlanCharacter::OnStartFire);
    PlayerInputComponent->BindAction("Fire", IE_Released, this, &AEscapePlanCharacter::OnStopFire);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AEscapePlanCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AEscapePlanCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AEscapePlanCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AEscapePlanCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AEscapePlanCharacter::OnResetVR);
}


void AEscapePlanCharacter::BeginPlay()
{
	// Call base class BeginPlay 
	Super::BeginPlay(); 
	// Spawn the weapon, if one was specified 
	if (WeaponClass) { 
		UWorld* World = GetWorld(); 
		if (World) { 
			FActorSpawnParameters SpawnParams; 
			SpawnParams.Owner = this; 
			SpawnParams.Instigator = Instigator; 
			// Need to set rotation like this because otherwise gun points down 
			FRotator Rotation(0.0f, 0.0f, -90.0f);

			// Spawn the Weapon 
			MyWeapon = World->SpawnActor<AWeapon>(WeaponClass, FVector::ZeroVector, Rotation, SpawnParams); 
			if (MyWeapon) { 
				// This is attached to "mWeaponPoint" which is defined in the skeleton
				MyWeapon->AttachToComponent(GetMesh(),
					FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponPoint"));
			}
            MyWeapon -> setOwner(this);
		} 
	}
}


void AEscapePlanCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AEscapePlanCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		Jump();
	}
}

void AEscapePlanCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		StopJumping();
	}
}

void AEscapePlanCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AEscapePlanCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AEscapePlanCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AEscapePlanCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AEscapePlanCharacter::takedamge(){
    myHealth -= 10.0f;
}

float AEscapePlanCharacter::getHealth() const{
    return myHealth;
}

float AEscapePlanCharacter::getMaxHealth() const{
    return maxHealth;
}

void AEscapePlanCharacter::OnStartFire(){
    if(MyWeapon != nullptr && myAmmo > 0){
        MyWeapon -> OnStartFire();
    }
    else{
        if(extraAmmo != 0){
            extraAmmo--;
            myAmmo = maxAmmo;
        }
    }
}

void AEscapePlanCharacter::OnStopFire(){
    if(MyWeapon != nullptr && myAmmo > 0){
        MyWeapon -> OnStopFire();
    }
    else{
        if(extraAmmo != 0){
            extraAmmo--;
            myAmmo = maxAmmo;
        }
    }
}

float AEscapePlanCharacter::TakeDamage(float Damage, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	if (ActualDamage > 0.0f) {
		myHealth -= ActualDamage;
		if (myHealth <= 0.0f) {
			// We're dead, don't allow further damage 
			bCanBeDamaged = false;
			// TODO: Process death 
			dead = true;
			OnStopFire();
			float duration = PlayAnimMontage(DeathAnim);
			GetWorldTimerManager().SetTimer(PlayerTimer, this, &AEscapePlanCharacter::ToDie, duration - 0.25f);
		}
	}
	return ActualDamage;
}

void AEscapePlanCharacter::ToDie()
{
	APlayerController* PC = Cast<APlayerController>(GetController());
	if (PC)
	{
		PC->SetCinematicMode(true, true, true);
	}
	GetMesh()->Deactivate();
}

void AEscapePlanCharacter::deductAmmo(){
    myAmmo--;
}

float AEscapePlanCharacter::getAmmo() const{
    return myAmmo;
}

void AEscapePlanCharacter::getkey(){
    getKey = true;
}

void AEscapePlanCharacter::addAmmo(){
    extraAmmo++;
}
