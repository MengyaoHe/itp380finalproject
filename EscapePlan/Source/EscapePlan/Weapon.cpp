// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "Weapon.h"
#include "EscapePlanCharacter.h"
#include "Enemy.h"
#include "Zombie.h"
#include "Sound/SoundCue.h"


// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh")); 
	RootComponent = WeaponMesh;
    FireRate = 0.1f;
    WeaponRange = 10000.0f;
    Damage = 5.0f;
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeapon::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

//Get the mesh of the weapon
USkeletalMeshComponent* AWeapon::GetWeaponMesh() const{
    return WeaponMesh;
}

UAudioComponent* AWeapon::PlayWeaponSound(USoundCue* Sound){
    UAudioComponent* AC = NULL;
    if(Sound){
        AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
    }
    return AC;
}

void AWeapon::OnStartFire(){
    FireAC = PlayWeaponSound(FireLoopSound);
    MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, WeaponMesh, TEXT("MuzzleFlashSocket"));
    GetWorldTimerManager().SetTimer(WeaponTimer, this, &AWeapon::WeaponTrace,FireRate, true);
}

void AWeapon::OnStopFire(){
    if(myOwner != nullptr && FireAC != nullptr){
        FireAC -> Stop();
        MuzzlePSC -> DeactivateSystem();
        PlayWeaponSound(FireFinishSound);
        GetWorldTimerManager().ClearTimer(WeaponTimer);
    }
}

class AEscapePlanCharacter* AWeapon::getOwner(){
    return myOwner;
}

void AWeapon::setOwner(AEscapePlanCharacter* owner){
    myOwner = owner;
}

void AWeapon::WeaponTrace(){
    static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
    static FName MuzzleSocket = FName(TEXT("MuzzleFlashSocket"));
    
    // Start from the muzzle's position
    FVector StartPos = WeaponMesh->GetSocketLocation(MuzzleSocket);
    // Get forward vector of MyPawn
    FVector Forward = myOwner -> GetActorForwardVector();
    // Calculate end position
    FVector EndPos = StartPos;
    EndPos += WeaponRange*Forward;
    
    //Perform trace to retrieve hit info
    FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
    TraceParams.bTraceAsyncScene = true;
    TraceParams.bReturnPhysicalMaterial = true;
    
    //This fires the ray and checks against all objects w/collision
    FHitResult Hit(ForceInit);
    GetWorld() -> LineTraceSingleByObjectType(Hit, StartPos, EndPos, FCollisionObjectQueryParams::AllObjects, TraceParams);
    
    //Did this hit anything
    if(Hit.bBlockingHit){
        UGameplayStatics::SpawnEmitterAtLocation(this, hitEffect, Hit.ImpactPoint);
		
		AZombie* Zombie = Cast<AZombie>(Hit.GetActor());
		if (Zombie) {
			Zombie->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
		}
		AEnemy* Dwarf = Cast<AEnemy>(Hit.GetActor());
		if (Dwarf) {
			Dwarf->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
		}
    }
    
    myOwner -> deductAmmo();
    if(myOwner -> getAmmo() == 0){
        OnStopFire();
    }
}
