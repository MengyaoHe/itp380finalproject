// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

UCLASS()
class ESCAPEPLAN_API AWeapon : public AActor
{
	GENERATED_BODY()

protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon) USkeletalMeshComponent* WeaponMesh;
    UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = Gun)
    float FireRate;
    
    UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = Gun)
    float WeaponRange;
    
    UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = Gun)
    float Damage;
    
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundCue* FireLoopSound;
    
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundCue* FireFinishSound;
    
    UPROPERTY(Transient)
    class UAudioComponent* FireAC;
    
    UPROPERTY(EditDefaultsOnly, Category = Effects)
    UParticleSystem* MuzzleFX;
    
    UPROPERTY(Transient)
    UParticleSystemComponent* MuzzlePSC;

    class AEscapePlanCharacter* myOwner;
    
    FTimerHandle WeaponTimer;
    
    UPROPERTY(EditDefaultsOnly)
    UParticleSystem* hitEffect;
    

public:
    // Sets default values for this actor's properties
    AWeapon();
    
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    // Called every frame
    virtual void Tick( float DeltaSeconds ) override;
    
    USkeletalMeshComponent* GetWeaponMesh() { return WeaponMesh; }
    
    USkeletalMeshComponent* GetWeaponMesh() const;
    
    UAudioComponent* PlayWeaponSound(USoundCue* Sound);
    
    void OnStartFire();
    
    void OnStopFire();
    
    class AEscapePlanCharacter* getOwner();
    
    void setOwner(class AEscapePlanCharacter* owner);
    
    void WeaponTrace();
};
