// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPEPLAN_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()

public:
	void BeginPlay() override;
	void Tick(float DeltaTime) override;
	void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) override;

private:
	enum State
	{
		Start,
		Chase,
		Attack,
		Dead
	};
	State state;

	UPROPERTY(EditAnywhere) float attackRange;
	APawn* pawnPlayer;
	APawn* pawnEnemy;
	float dis;
	
	
};
