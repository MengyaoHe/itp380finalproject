#pragma once
#include "GameFramework/Character.h"
#include "EscapePlanCharacter.generated.h"

UCLASS(config=Game)
class AEscapePlanCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AEscapePlanCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
    
    /* when the actor takes damage */
    void takedamge();
    
    float getHealth() const;
    
    float getMaxHealth() const;
    
    UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = Damage) float myHealth = 0.00f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Damage) float maxHealth = 100.00f;
    UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = Ammo) float myAmmo = 230.00f;
    UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = Ammo) float maxAmmo = 230.00f;

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

protected:
	UPROPERTY(EditAnywhere, Category = Weapon) TSubclassOf<class AWeapon> WeaponClass;

private:
	class AWeapon* MyWeapon;

	UPROPERTY(EditDefaultsOnly) UAnimMontage* DeathAnim;
	bool IsDead() { return dead; }
	bool dead = false;
	void ToDie();
	FTimerHandle PlayerTimer;
    bool getKey = false;
    float extraAmmo = 0;
public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	void BeginPlay() override;
    
    void OnStartFire();
    
    void OnStopFire();

	float TakeDamage(float Damage, FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override;
    
    void deductAmmo();
    
    float getAmmo() const;
    
    void getkey();
    
    void addAmmo();
};

